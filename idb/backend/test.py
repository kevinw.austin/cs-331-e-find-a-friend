import os
import sys
import unittest
from models import db, Dog, Cat, Bird

class DBTestCases(unittest.TestCase):
    
    def test_dog1(self):
        dog = Dog(breed='husky',
            id=100,
            name='doggo',
            gender='male',
            color='grey',
            img_url='placeholder_url',
            age='10'
        )
        db.session.add(dog)
        db.session.commit()

        doggobase = db.session.query(Dog).filter_by(id = 100).one()
        self.assertEqual(int(doggobase.id), 100)
        self.assertEqual(str(doggobase.breed), 'husky')
        self.assertEqual(str(doggobase.name), 'doggo')
        self.assertEqual(str(doggobase.gender), 'male')
        self.assertEqual(str(doggobase.color), 'grey')
        self.assertEqual(str(doggobase.img_url), 'placeholder_url')
        self.assertEqual(str(doggobase.age), '10')

        db.session.query(Dog).filter_by(id = 100).delete()
        db.session.commit()
    
    def test_dog2(self):
        dog = Dog(breed='',
            id=0,
            name='',
            gender='',
            color='',
            img_url='',
            age=''
        )
        db.session.add(dog)
        db.session.commit()

        doggobase = db.session.query(Dog).filter_by(id = 0).one()
        self.assertEqual(int(doggobase.id), 0)
        self.assertEqual(str(doggobase.breed), '')
        self.assertEqual(str(doggobase.name), '')
        self.assertEqual(str(doggobase.gender), '')
        self.assertEqual(str(doggobase.color), '')
        self.assertEqual(str(doggobase.img_url), '')
        self.assertEqual(str(doggobase.age), '')

        db.session.query(Dog).filter_by(id = 0).delete()
        db.session.commit()
    
    def test_dog3(self):
        dog = Dog(breed='breed',
            id=12394812,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(dog)
        db.session.commit()

        doggobase = db.session.query(Dog).filter_by(id = 12394812).one()
        self.assertEqual(int(doggobase.id), 12394812)
        self.assertEqual(str(doggobase.breed), 'breed')
        self.assertEqual(str(doggobase.name), 'name')
        self.assertEqual(str(doggobase.gender), 'gender')
        self.assertEqual(str(doggobase.color), 'color')
        self.assertEqual(str(doggobase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(doggobase.age), '1234')

        db.session.query(Dog).filter_by(id = 12394812).delete()
        db.session.commit()
    
    def test_dog4(self):
        dog = Dog(breed='breed',
            id=85274496,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(dog)
        db.session.commit()

        doggobase = db.session.query(Dog).filter_by(id = 85274496).one()
        self.assertEqual(int(doggobase.id), 85274496)
        self.assertEqual(str(doggobase.breed), 'breed')
        self.assertEqual(str(doggobase.name), 'name')
        self.assertEqual(str(doggobase.gender), 'gender')
        self.assertEqual(str(doggobase.color), 'color')
        self.assertEqual(str(doggobase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(doggobase.age), '1234')

        db.session.query(Dog).filter_by(id = 85274496).delete()
        db.session.commit()

        dog = Dog(breed='breed',
            id=85274496,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(dog)
        db.session.commit()

        doggobase = db.session.query(Dog).filter_by(id = 85274496).one()
        self.assertEqual(int(doggobase.id), 85274496)
        self.assertEqual(str(doggobase.breed), 'breed')
        self.assertEqual(str(doggobase.name), 'name')
        self.assertEqual(str(doggobase.gender), 'gender')
        self.assertEqual(str(doggobase.color), 'color')
        self.assertEqual(str(doggobase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(doggobase.age), '1234')

        db.session.query(Dog).filter_by(id = 85274496).delete()
        db.session.commit()
    
    def test_dog5(self):
        dog = Dog(breed='breed',
            id=48629713,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        
        dog1 = Dog(breed='breed1',
            id=48629714,
            name='name1',
            gender='gender1',
            color='color1',
            img_url='@#$%&^&*(()*&&^%""1',
            age=1235
        )
        db.session.add(dog)
        db.session.add(dog1)
        db.session.commit()

        doggobase = db.session.query(Dog).filter_by(id = 48629713).one()
        self.assertEqual(int(doggobase.id), 48629713)
        self.assertEqual(str(doggobase.breed), 'breed')
        self.assertEqual(str(doggobase.name), 'name')
        self.assertEqual(str(doggobase.gender), 'gender')
        self.assertEqual(str(doggobase.color), 'color')
        self.assertEqual(str(doggobase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(doggobase.age), '1234')

        doggobase = db.session.query(Dog).filter_by(id = 48629714).one()
        self.assertEqual(int(doggobase.id), 48629714)
        self.assertEqual(str(doggobase.breed), 'breed1')
        self.assertEqual(str(doggobase.name), 'name1')
        self.assertEqual(str(doggobase.gender), 'gender1')
        self.assertEqual(str(doggobase.color), 'color1')
        self.assertEqual(str(doggobase.img_url), '@#$%&^&*(()*&&^%""1')
        self.assertEqual(str(doggobase.age), '1235')

        db.session.query(Dog).filter_by(id = 48629713).delete()
        db.session.query(Dog).filter_by(id = 48629714).delete()
        db.session.commit()
    
    def test_cat1(self):
        cat = Cat(breed='shorthair',
            id=101,
            name='catto',
            gender='female',
            color='yellow',
            img_url='cat_pic',
            age=11
        )
        db.session.add(cat)
        db.session.commit()

        catabase = db.session.query(Cat).filter_by(id = 101).one()
        self.assertEqual(int(catabase.id), 101)
        self.assertEqual(str(catabase.breed), 'shorthair')
        self.assertEqual(str(catabase.name), 'catto')
        self.assertEqual(str(catabase.gender), 'female')
        self.assertEqual(str(catabase.color), 'yellow')
        self.assertEqual(str(catabase.img_url), 'cat_pic')
        self.assertEqual(str(catabase.age), '11')

        db.session.query(Cat).filter_by(id = 101).delete()
        db.session.commit()
    
    def test_cat2(self):
        cat = Cat(breed='',
            id=0,
            name='',
            gender='',
            color='',
            img_url='',
            age=''
        )
        db.session.add(cat)
        db.session.commit()

        catabase = db.session.query(Cat).filter_by(id = 0).one()
        self.assertEqual(int(catabase.id), 0)
        self.assertEqual(str(catabase.breed), '')
        self.assertEqual(str(catabase.name), '')
        self.assertEqual(str(catabase.gender), '')
        self.assertEqual(str(catabase.color), '')
        self.assertEqual(str(catabase.img_url), '')
        self.assertEqual(str(catabase.age), '')

        db.session.query(Cat).filter_by(id = 0).delete()
        db.session.commit()
    
    def test_cat3(self):
        cat = Cat(breed='breed',
            id=12394812,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(cat)
        db.session.commit()

        catabase = db.session.query(Cat).filter_by(id = 12394812).one()
        self.assertEqual(int(catabase.id), 12394812)
        self.assertEqual(str(catabase.breed), 'breed')
        self.assertEqual(str(catabase.name), 'name')
        self.assertEqual(str(catabase.gender), 'gender')
        self.assertEqual(str(catabase.color), 'color')
        self.assertEqual(str(catabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(catabase.age), '1234')

        db.session.query(Cat).filter_by(id = 12394812).delete()
        db.session.commit()
    
    def test_cat4(self):
        cat = Cat(breed='breed',
            id=75684952,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(cat)
        db.session.commit()

        catabase = db.session.query(Cat).filter_by(id = 75684952).one()
        self.assertEqual(int(catabase.id), 75684952)
        self.assertEqual(str(catabase.breed), 'breed')
        self.assertEqual(str(catabase.name), 'name')
        self.assertEqual(str(catabase.gender), 'gender')
        self.assertEqual(str(catabase.color), 'color')
        self.assertEqual(str(catabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(catabase.age), '1234')

        db.session.query(Cat).filter_by(id = 75684952).delete()
        db.session.commit()

        cat = Cat(breed='breed',
            id=75684952,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(cat)
        db.session.commit()

        catabase = db.session.query(Cat).filter_by(id = 75684952).one()
        self.assertEqual(int(catabase.id), 75684952)
        self.assertEqual(str(catabase.breed), 'breed')
        self.assertEqual(str(catabase.name), 'name')
        self.assertEqual(str(catabase.gender), 'gender')
        self.assertEqual(str(catabase.color), 'color')
        self.assertEqual(str(catabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(catabase.age), '1234')

        db.session.query(Cat).filter_by(id = 75684952).delete()
        db.session.commit()
    
    def test_cat5(self):
        cat = Cat(breed='breed',
            id=65432198,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )

        cat1 = Cat(breed='breed1',
            id=65432199,
            name='name1',
            gender='gender1',
            color='color1',
            img_url='@#$%&^&*(()*&&^%""1',
            age=1235
        )
        db.session.add(cat)
        db.session.add(cat1)
        db.session.commit()

        catabase = db.session.query(Cat).filter_by(id = 65432198).one()
        self.assertEqual(int(catabase.id), 65432198)
        self.assertEqual(str(catabase.breed), 'breed')
        self.assertEqual(str(catabase.name), 'name')
        self.assertEqual(str(catabase.gender), 'gender')
        self.assertEqual(str(catabase.color), 'color')
        self.assertEqual(str(catabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(catabase.age), '1234')

        catabase = db.session.query(Cat).filter_by(id = 65432199).one()
        self.assertEqual(int(catabase.id), 65432199)
        self.assertEqual(str(catabase.breed), 'breed1')
        self.assertEqual(str(catabase.name), 'name1')
        self.assertEqual(str(catabase.gender), 'gender1')
        self.assertEqual(str(catabase.color), 'color1')
        self.assertEqual(str(catabase.img_url), '@#$%&^&*(()*&&^%""1')
        self.assertEqual(str(catabase.age), '1235')

        db.session.query(Cat).filter_by(id = 65432198).delete()
        db.session.query(Cat).filter_by(id = 65432199).delete()
        db.session.commit()

    def test_bird1(self):
        bird = Bird(breed='parrot',
            id=102,
            name='birdo',
            gender='male',
            color='white',
            img_url='chirp.png',
            age=12
        )
        db.session.add(bird)
        db.session.commit()

        birdabase = db.session.query(Bird).filter_by(id = 102).one()
        self.assertEqual(int(birdabase.id), 102)
        self.assertEqual(str(birdabase.breed), 'parrot')
        self.assertEqual(str(birdabase.name), 'birdo')
        self.assertEqual(str(birdabase.gender), 'male')
        self.assertEqual(str(birdabase.color), 'white')
        self.assertEqual(str(birdabase.img_url), 'chirp.png')
        self.assertEqual(str(birdabase.age), '12')

        db.session.query(Bird).filter_by(id = 102).delete()
        db.session.commit()
    
    def test_bird2(self):
        bird = Bird(breed='',
            id=0,
            name='',
            gender='',
            color='',
            img_url='',
            age=''
        )
        db.session.add(bird)
        db.session.commit()

        birdabase = db.session.query(Bird).filter_by(id = 0).one()
        self.assertEqual(int(birdabase.id), 0)
        self.assertEqual(str(birdabase.breed), '')
        self.assertEqual(str(birdabase.name), '')
        self.assertEqual(str(birdabase.gender), '')
        self.assertEqual(str(birdabase.color), '')
        self.assertEqual(str(birdabase.img_url), '')
        self.assertEqual(str(birdabase.age), '')

        db.session.query(Bird).filter_by(id = 0).delete()
        db.session.commit()
    
    def test_bird3(self):
        bird = Bird(breed='breed',
            id=12394812,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(bird)
        db.session.commit()

        birdabase = db.session.query(Bird).filter_by(id = 12394812).one()
        self.assertEqual(int(birdabase.id), 12394812)
        self.assertEqual(str(birdabase.breed), 'breed')
        self.assertEqual(str(birdabase.name), 'name')
        self.assertEqual(str(birdabase.gender), 'gender')
        self.assertEqual(str(birdabase.color), 'color')
        self.assertEqual(str(birdabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(birdabase.age), '1234')

        db.session.query(Bird).filter_by(id = 12394812).delete()
        db.session.commit()
    
    def test_bird4(self):
        bird = Bird(breed='breed',
            id=16478865,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(bird)
        db.session.commit()

        birdabase = db.session.query(Bird).filter_by(id = 16478865).one()
        self.assertEqual(int(birdabase.id), 16478865)
        self.assertEqual(str(birdabase.breed), 'breed')
        self.assertEqual(str(birdabase.name), 'name')
        self.assertEqual(str(birdabase.gender), 'gender')
        self.assertEqual(str(birdabase.color), 'color')
        self.assertEqual(str(birdabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(birdabase.age), '1234')

        db.session.query(Bird).filter_by(id = 16478865).delete()
        db.session.commit()

        bird = Bird(breed='breed',
            id=16478865,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )
        db.session.add(bird)
        db.session.commit()

        birdabase = db.session.query(Bird).filter_by(id = 16478865).one()
        self.assertEqual(int(birdabase.id), 16478865)
        self.assertEqual(str(birdabase.breed), 'breed')
        self.assertEqual(str(birdabase.name), 'name')
        self.assertEqual(str(birdabase.gender), 'gender')
        self.assertEqual(str(birdabase.color), 'color')
        self.assertEqual(str(birdabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(birdabase.age), '1234')

        db.session.query(Bird).filter_by(id = 16478865).delete()
        db.session.commit()
    
    def test_bird5(self):
        bird = Bird(breed='breed',
            id=87159854,
            name='name',
            gender='gender',
            color='color',
            img_url='@#$%&^&*(()*&&^%""',
            age=1234
        )

        bird1 = Bird(breed='breed1',
            id=87159855,
            name='name1',
            gender='gender1',
            color='color1',
            img_url='@#$%&^&*(()*&&^%""1',
            age=1235
        )
        db.session.add(bird)
        db.session.add(bird1)
        db.session.commit()

        birdabase = db.session.query(Bird).filter_by(id = 87159855).one()
        self.assertEqual(int(birdabase.id), 87159855)
        self.assertEqual(str(birdabase.breed), 'breed1')
        self.assertEqual(str(birdabase.name), 'name1')
        self.assertEqual(str(birdabase.gender), 'gender1')
        self.assertEqual(str(birdabase.color), 'color1')
        self.assertEqual(str(birdabase.img_url), '@#$%&^&*(()*&&^%""1')
        self.assertEqual(str(birdabase.age), '1235')

        birdabase = db.session.query(Bird).filter_by(id = 87159854).one()
        self.assertEqual(int(birdabase.id), 87159854)
        self.assertEqual(str(birdabase.breed), 'breed')
        self.assertEqual(str(birdabase.name), 'name')
        self.assertEqual(str(birdabase.gender), 'gender')
        self.assertEqual(str(birdabase.color), 'color')
        self.assertEqual(str(birdabase.img_url), '@#$%&^&*(()*&&^%""')
        self.assertEqual(str(birdabase.age), '1234')

        db.session.query(Bird).filter_by(id = 87159854).delete()
        db.session.query(Bird).filter_by(id = 87159855).delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()