import React from "react"
import haroon from "../../Photos/haroon.jpg"
import matthew from "../../Photos/matthew.png"
import milan from "../../Photos/milan.jpg"
import kevin from "../../Photos/kevin.JPG"
import ryan from "../../Photos/ryan.jpg"
import unittest from "../../Photos/unittest_results.PNG"
  
function ContactUs(){
    return(
    <div className="bg-dark">
        <div bg="dark">
            <h1 style={{color: '#ffffff'}}>We Are Find-a-Friend!</h1>
            <br/><br/>
        <div>
        <div style={{color: "white"}}>
        <h2>Haroon Dossani</h2>
                <div style={{float:"left"}}>
                    <img src={haroon} style={{height: "230pt", width: "200pt"}}alt="Imagehere"></img>
                </div>
                <table>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Bio: </b><p>I am a Junior Biochemistry major at the University of Texas at Austin. I am from Austin, TX.</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Responsibilities: </b><p>Front end development with React</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Issues: </b>9</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Commits: </b>28</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Unit Tests: </b>3</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><a href="https://www.linkedin.com/in/haroon-dossani-81a64b197/" class="button">LinkedIn</a>----<a href="https://www.cs.utexas.edu/~haroon/" class="button">CS Profile</a></td>
                    </tr>
                </table>    
        </div>
        <br></br><br></br><br></br><br></br>
        <div style={{color: "white"}}>
        <h2>Milan Dean</h2>
                <div style={{float:"left"}}>
                    <img src={milan} style={{width: "200pt"}}alt="Imagehere"></img>
                </div>
                <table>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Bio: </b><p> Current Senior in Mathematics at The University of Texas at Austin </p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Responsibilities: </b><p>Front end development with React</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Issues: </b>1</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Commits: </b>27</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Unit Tests: </b>3</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><a href="https://www.linkedin.com/in/milan-dean-6b625a98/" class="button">LinkedIn</a>----<a href="https://www.cs.utexas.edu/users/miland/" class="button">CS Profile</a></td>
                    </tr>
                </table>      
        </div>
        <br></br><br></br><br></br><br></br><br></br>
        <div style={{color: "white"}}>
        <h2>Matthew Umana</h2>
                <div style={{float:"left"}}>
                    <img src={matthew} style={{width: "200pt"}}alt="Imagehere"></img>
                </div>
                <table>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Bio:</b><p> I am a senior Entertainment Technology major at the University of Texas at Austin.</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Responsibilities: </b><p>Frontend / Backend Development<br/>GCP Deployment</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Issues: </b>2</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Commits: </b>35</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Unit Tests: </b>3</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><a href="https://www.linkedin.com/in/matthew-umana" class="button">LinkedIn</a>----<a href="https://www.cs.utexas.edu/~umana/" class="button">CS Profile</a></td>
                    </tr>
                </table>      
        </div>
        <br></br><br></br><br></br><br></br><br></br>
        <div style={{color: "white"}}>
        <h2>Kevin Wang</h2>
                <div style={{float:"left"}}>
                    <img src={kevin} style={{width: "200pt"}}alt="Imagehere"></img>
                </div>
                <table>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Bio: </b><p>I am a junior Economics major at the University of Texas at Austin, with minors in Computer Science and Finance.</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Responsibilities: </b><p>Frontend / Backend Development</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Issues: </b>1</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Commits: </b>11</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Unit Tests: </b>3</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><a href="https://www.linkedin.com/in/kevinwaustin" class="button">LinkedIn</a>----<a href="https://www.cs.utexas.edu/~kmw4287/" class="button">CS Profile</a></td>
                    </tr>
                </table>           
        </div>
        <br></br><br></br><br></br><br></br><br></br>
        <div style={{color: "white"}}>
        <h2>Ryan Friedewald</h2>
                <div style={{float:"left"}}>
                    <img src={ryan} style={{height: "230pt", width: "200pt"}}alt="Imagehere"></img>
                </div>
                <table>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Bio: </b><p>I am a senior Economics major with a minor in Computer Science.</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>Responsibilities: </b><p>Front end development with React</p></td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Issues: </b>10</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Commits: </b>10</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><b>No. of Unit Tests: </b>3</td>
                    </tr>
                    <tr>
                        <td style={{paddingLeft: "75pt"}}><a href="https://www.linkedin.com/in/ryan-friedewald-05075812b/" class="button">LinkedIn</a>----<a href="https://www.cs.utexas.edu/~rfried/" class="button">CS Profile</a></td>
                    </tr>
                </table>        
        </div>
        
        
    </div>
        
        
    </div>
    <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
            <footer>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h2>Data Sources Used: </h2>
                                <p>RescueGroups: https://userguide.rescuegroups.org/display/APIDG/API+Developers+Guide+Home</p>
                                <p>PetFinder: https://www.petfinder.com/developers/</p>
                                <p>Adopt-a-Pet: https://www.adoptapet.com/public/apis/pet_list.html</p>
                                <h3>Tools Used: </h3>
                                <p>Python, Flask, React, HTML, Bootstrap, SQLAlchemy, PostgreSQL 12</p>
                                <h2>Stats:</h2>
                                <p>Total No. of Commits: 120</p>
                                <p>Total No. of Issues: 45</p>
                                <p>
                                    Total No. of Unittests: 15
                                    <br/><img alt="unit test results" src={unittest}/>
                                </p>
                                <a href={"https://gitlab.com/hdoss99/idb/-/issues"}>Gitlab Issue Tracker</a><br></br>
                                <a href={"https://gitlab.com/hdoss99/idb/-/tree/master"}>Gitlab Repository</a><br></br>
                                <a href={"https://gitlab.com/hdoss99/idb/-/wikis/home"}>Gitlab Wiki</a><br></br>
                                <a href={"https://speakerdeck.com/haroon1999/find-a-friend"}>SpeakerDeck Presentation</a><br></br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </footer>
    </div>
    )

}
export default ContactUs

{/* <BioCard person={{name: "Haroon Dossani", imgSRC: {doge}, bio: "I am a Junior Biochemistry major in the Polymathic Scholars Honors Program at the University of Texas at Austin. I am from Austin, TX.", 
                responsibilities: "Front end development with React", numCommits: "1", 
                numIssues: "3", numUnitTests: "0"
            }} />
            <BioCard person={{name: "Milan Dean", imgSrc: {doge}, bio: "Bio Here", 
                responsibilities: "Front end development with React", numCommits: "1", 
                numIssues: "3", numUnitTests: "0"
            }} />
            <BioCard person={{name: "Matthew Umana", imgSrc: {doge}, bio: "Bio Here", 
                responsibilities: "Front end development with React", numCommits: "1", 
                numIssues: "3", numUnitTests: "0"
            }} />
            <BioCard person={{name: "Kevin Wang", imgSrc: {doge}, bio: "Bio Here", 
                responsibilities: "Front end development with React", numCommits: "1", 
                numIssues: "3", numUnitTests: "0"
            }} />
            <BioCard person={{name: "Ryan Friedewald", imgSrc: {doge}, bio: "Bio Here", 
                responsibilities: "Front end development with React", numCommits: "1", 
                numIssues: "3", numUnitTests: "0"
            }} /> */}