import React from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import { ArrowDownUp } from 'react-bootstrap-icons'

let order = 'desc';
class Birds extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            all_bird_data: [],
            bird_data: [],
            isLoaded: false,
        }
    }

    async getData(){
        await fetch('https://find-a-friend-306014.uc.r.appspot.com/api/birds') 
        .then(results => results.json())
        .then(data => {
            let filteredData = this.filterData(data)
            this.setState({
                all_bird_data: data,
                bird_data: filteredData["Birds"],
                isLoaded: true
            })
        })
        console.log(this.state.all_bird_data)
        console.log(this.state.bird_data)
    }

    async componentDidMount(){
        this.getData()
    }

    componentDidUpdate(nextProps) {
        if (nextProps !== this.props) {
            this.getData()
        }
    }

    filterData(data){
        let newData = {"Birds": []}
        data["Birds"].forEach(bird => {
            if(bird.breed != this.props.breed && 
                this.props.breed != "" && 
                bird.breed.includes(this.capitalizeFirstLetter(this.props.breed)) == false ){
                return
            }
            if(bird.color == null && this.props.color != ""){
                return
            }
            if(bird.color != this.props.color && 
                this.props.color != "" && 
                this.containsString(bird.color, this.props.color) == false 
                ){
                console.log(this.capitalizeFirstLetter(this.props.color))
                return
            }
            if(bird.age != this.props.age && this.props.age != "All"){
                return
            }
            if(bird.gender != this.props.gender && this.props.gender != "All"){
                return
            }
            newData["Birds"].push(bird)
        })
        return newData
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    containsString(stringOne, stringTwo){
        // if(stringOne === null && stringTwo === ""){
        //     return true
        // }
        // else{
            return stringOne.includes(this.capitalizeFirstLetter(stringTwo))
        // }
    }

    handleBtnClick = () => {
        if (order === 'desc') {
        this.table.handleSort('asc', 'name');
        order = 'asc';
        } else {
        this.table.handleSort('desc', 'name');
        order = 'desc';
        }
    }

    imageFormatter(cell, row){
        return (<a href={row.url}><img style={{width:'100%'}} src={cell}/></a>)
    }

    render() {

        let {isLoaded, data} = this.state

        if(!isLoaded){
            return <div>Loading...</div>
        }

        else{
            return (
            <div>
                <BootstrapTable ref='table' data={ this.state.bird_data } className="animalTable bg-dark" pagination>
                    <TableHeaderColumn dataField='img' isKey={ true } dataFormat={this.imageFormatter}>Picture/Link</TableHeaderColumn>
                    <TableHeaderColumn dataField='name' dataSort={ true }>Name <ArrowDownUp/></TableHeaderColumn>
                    <TableHeaderColumn dataField='breed' dataSort={ true }>Breed <ArrowDownUp/></TableHeaderColumn>
                    <TableHeaderColumn dataField='color' dataSort={ true }>Color <ArrowDownUp/></TableHeaderColumn>
                    <TableHeaderColumn dataField='age' dataSort={ true }>Age <ArrowDownUp/></TableHeaderColumn>
                    <TableHeaderColumn dataField='gender' dataSort={ true }>Gender <ArrowDownUp/></TableHeaderColumn>
                </BootstrapTable>
            </div>
            )
        }
    }
}

export default Birds