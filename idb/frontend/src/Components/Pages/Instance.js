import React from "react"
import Carousel from 'react-bootstrap/Carousel'
import {Col, Container, Row} from "react-bootstrap"
import {useParams} from 'react-router-dom'

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

function Instance(){

    const imgstyle = {
        display:"block",
        height:400,
        marginLeft:"auto",
        marginRight:"auto"
      }
    
    const parastyle = {
        color:'white', 
        fontSize:22, 
        paddingLeft:300, 
        paddingTop:25, 
        paddingRight:300, 
        fontFamily:"Arial"
    }

    let {species} = useParams()
    let {id} = useParams()

    let dogList = [{
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20210223065843.jpeg", 
            "https://www.austinpetsalive.org/assets/animals/_gallery/20200404201815_0_0.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210224204427.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210116225610.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20200404201727.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20200322112756_0.jpeg"
            ],
        name: "Hooch",
        color: "Black/Brown",
        breed: "Black Labrador/Retriever",
        age: 7,
        gender: "Male",
        link: "https://www.austinpetsalive.org/adopt/dogs/apa-a-27112",
        blurb: "Hooch would love to be your new best friend! This sweet boy loves to be everywhere you are. He's perfectly happy following you around all day and spending as much time with you as he can! Just give him a bed and a window and he'll be your best co-worker and power napper!",
        id: 0
    }, {
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20210221120703.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20191216225908.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210221121105.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210221120916.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210221120814.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20191216225758.jpeg"
            ],
        name: "Noodle",
        color: "Black/White",
        breed: "Chinese Shar-pei Mix",
        age: 4,
        gender: "Female",
        link: "https://www.austinpetsalive.org/adopt/dogs/apa-a-54290",
        blurb: "Noodle has a beautiful face complemented with her adorable Shar Pei snout. She loves hard and plays hard and snorts and grunts when she's all riled up. She loves to run as fast as she can around the coffee table and then stop abruptly, which is your cue to start chasing her!",
        id: 1
    }, {
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20201115192009.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210126092951.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210126092923.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210126092821.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210115061207.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210110220024.jpeg"
            ],
        name: "Achilles",
        color: "Brown",
        breed: "Bulldog Mix",
        age: 8,
        gender: "Male",
        link: "https://www.austinpetsalive.org/adopt/dogs/apa-a-56667",
        blurb: "Achilles was found as a stray, but he has so much affection for people that we think he may not have always been on his own. He constantly wants to snuggle and when he sees his people coming, he wags his little tail with all his might! He loves to be by your side and will lay his big adorable head on your leg Or lean his whole body against you. His APA! friends rave about his kisses and how speedy and sneaky he is with them.",
        id: 2
    }, {
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20210221192040.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210223070704.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210223070653.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210221192013.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210110212830.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210110212657.jpeg"
            ],
        name: "Barney",
        color: "Black",
        breed: "German Shepherd Mix",
        age: 1,
        gender: "Male",
        link: "https://www.austinpetsalive.org/adopt/dogs/apa-a-77767",
        blurb: "If you want to become Barney's friend, then come visit him, you'll find this big puppy wiggling in your lap - and giving you sweet sloppy kisses. He'll be so happy to be your new best friend, snuggling, and asking for cuddles. He will want to play fetch and tug-o-war with you.",
        id: 3
    }]

    let catList = [{
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20180420202515.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20180420202446.jpeg"],
        name: "Gold Rush",
        color: "White/Gold",
        breed: "Domestic Shorthair",
        age: 3,
        gender: "Female",
        link: "https://www.austinpetsalive.org/adopt/cats/apa-a-58002",
        blurb: "Gold Rush is the sweetest little ball of fur who loves to jump, play, and give snuggles and kisses. She absolutely adores her human companions and can lay around all day getting love and cuddles - when she isn't playing with her toy balls and kitty friend, Wanda.",
        id: 0
    }, {
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20190827140413.jpeg"],
        name: "Menlo",
        color: "Gray",
        breed: "Russian Blue/Mix",
        age: 2,
        gender: "Male",
        link: "https://www.austinpetsalive.org/adopt/cats/apa-a-72024",
        blurb: "Menlo is a gorgeous Russian Blue mix! He is a pretty chill guy, happy to look out the window. He gets along with other cats, and in fact, likes them very much! He is still a young cat and very interested in playing, as well as being held.",
        id: 1
    }, {
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20210127141607.jpeg",
            "https://www.austinpetsalive.org/assets/animals/_gallery/20210127141619.jpeg"],
        name: "Pricilla",
        color: "Light Gray",
        breed: "Siamese",
        age: 1,
        gender: "Female",
        link: "https://www.austinpetsalive.org/adopt/cats/apa-a-89748",
        blurb: "There's a new kitty on the block, Pricilla! She is busy unpacking her toys right now. But, once Pricilla is settled in, we know she will have stories to tell. We'll be sure to share all the purry details with you. Check back soon to get the full scoop, or, write adopt@austinpetsalive.org to connect with Pricilla's foster today!",
        id: 2
    }, {
        pictures: ["https://www.austinpetsalive.org/assets/animals/_gallery/20200811162201_0_0.jpeg", 
            "https://www.austinpetsalive.org/assets/animals/_gallery/20200811162250.jpeg"],
        name: "Charlie",
        color: "WhiteBlack",
        breed: "Domestic Shorthair",
        age: 5,
        gender: "Male",
        link: "https://www.austinpetsalive.org/adopt/cats/apa-a-36364",
        blurb: "Charlie is a friendly kitty whose handsome black and white coat may just remind you of an Oreo. He's every bit as sweet and definitely way more cuddly! His loving snuggles are absolutely the BEST! This affectionate boy is good with children and loves playing with toys. Charlie doesn't care for dogs, or other cats. He's be happiest as the only pet in your family. Contact us for a virtual introduction!",
        id: 3
    }]

    let birdList = [{
        pictures: ["https://bird-haven.org/wp-content/uploads/sunny-3.jpg"],
        name: "Sunny",
        color: "Yellow",
        breed: "Indian Ringneck",
        age: 10,
        gender: "Female",
        link: "https://bird-haven.org/adopt-me/",
        blurb: "Hello there. My name is Sunny. I am a really beautiful ringneck parakeet. My previous owner died so I came to the Haven to find a new home.",
        id: 0
    }, {
        pictures: ["https://bird-haven.org/wp-content/uploads/keto-225x300.jpg"],
        name: "Keto",
        color: "Gray",
        breed: "Congo African Grey",
        age: 21,
        gender: "Male",
        link: "https://bird-haven.org/adopt-me/",
        blurb: "Keto, also affectionately known as \"birdbrain\" is a 21-year old African Grey parrot. His very FAVORITE phrase is \"come on!\" and then he scratches his OWN head! He also says (with the Texas drawl!) \"Come O-o-o-o-oone!\" He's been pretty chatty in the bird room lately- not loud, just conversational. He's told the volunteers \"hello!\", \"Keto\" and helpfully, \"what are you doin\'?\"",
        id: 1
    }, {
        pictures: ["https://bird-haven.org/wp-content/uploads/toby-1.jpg"],
        name: "Buster",
        color: "Green",
        breed: "Blue Fronted Amazon",
        age: 12,
        gender: "Male",
        link: "https://bird-haven.org/adopt-me/",
        blurb: "Buster is a Blue Fronted Amazon. He isn't very hand tame but is very entertaining. He loves to greet you when you come in by saying Hello. He also likes to say \"ba-bye\" even before you are ready to leave.",
        id: 2
    }, {
        pictures: ["https://bird-haven.org/wp-content/uploads/sophie-1.jpg"],
        name: "Sophie",
        color: "White",
        breed: "Sulphur Crested Cockatoo",
        age: 25,
        gender: "Female",
        link: "https://bird-haven.org/adopt-me/",
        blurb: "Hello. My name is Sophie. I am one of the sweetest birds you've ever met. I love people. I am however quite loud.",
        id: 3
    }]

    var animalList
    var tempID
    const featuredNames = []
    const featuredIMGs = []
    if (species == "dogs") {
        animalList=dogList
        tempID = getRandomInt(catList.length)
        featuredNames.push(<Col><h5><a href={'/cats/' + tempID}>{catList[tempID]['name']}</a></h5></Col>)
        featuredIMGs.push(<Col><img style={imgstyle} src={catList[tempID]['pictures'][0]}/></Col>)
        tempID = getRandomInt(birdList.length)
        featuredNames.push(<Col><h5><a href={'/birds/' + tempID}>{birdList[tempID]['name']}</a></h5></Col>)
        featuredIMGs.push(<Col><img style={imgstyle} src={birdList[tempID]['pictures'][0]}/></Col>)
    } else if (species == "cats") {
        animalList=catList
        tempID = getRandomInt(dogList.length)
        featuredNames.push(<Col><h5><a href={'/dogs/' + tempID}>{dogList[tempID]['name']}</a></h5></Col>)
        featuredIMGs.push(<Col><img style={imgstyle} src={dogList[tempID]['pictures'][0]}/></Col>)
        tempID = getRandomInt(birdList.length)
        featuredNames.push(<Col><h5><a href={'/birds/' + tempID}>{birdList[tempID]['name']}</a></h5></Col>)
        featuredIMGs.push(<Col><img style={imgstyle} src={birdList[tempID]['pictures'][0]}/></Col>)
    } else if (species == "birds") {
        animalList=birdList
        tempID = getRandomInt(catList.length)
        featuredNames.push(<Col><h5><a href={'/cats/' + tempID}>{catList[tempID]['name']}</a></h5></Col>)
        featuredIMGs.push(<Col><img style={imgstyle} src={catList[tempID]['pictures'][0]}/></Col>)
        tempID = getRandomInt(dogList.length)
        featuredNames.push(<Col><h5><a href={'/dogs/' + tempID}>{dogList[tempID]['name']}</a></h5></Col>)
        featuredIMGs.push(<Col><img style={imgstyle} src={dogList[tempID]['pictures'][0]}/></Col>)
    }

    var i
    const pictureSlides = []
    for (i = 0; i<animalList[id]['pictures'].length; i++){
        pictureSlides.push(
            <Carousel.Item> 
                <img style={imgstyle} src={animalList[id]['pictures'][i]}/>
            </Carousel.Item>
        )
    }

    return(
        <div className="bg-dark">
            <div bg="dark">
                <h1 style={{color:'white', paddingLeft:100, paddingTop:25}}>Meet {animalList[id]['name']}!</h1>
                <br/><br/>
            </div>
            <Container fluid>
                <Row>
                    <Col sm='1'></Col>
                    <Col>
                        <Carousel>
                            {pictureSlides}
                        </Carousel>
                    </Col>
                    <Col sm='1'></Col>
                </Row>
                <Row style={{color:'white', textAlign:'center', fontSize:27, paddingTop:25}} className="justify-content-md-center">
                    <Col md='auto'>Name: {animalList[id]['name']}</Col>
                    <Col md='auto'>Color: {animalList[id]['color']}</Col>
                    <Col md='auto'>Breed: {animalList[id]['breed']}</Col>
                    <Col md='auto'>Age: {animalList[id]['age']}</Col>
                    <Col md='auto'>Gender: {animalList[id]['gender']}</Col>
                </Row>
            </Container>             
            
            <p style={parastyle}>
                {animalList[id]['blurb']}
            </p>
                <h2 style={{color:"BlanchedAlmond", textAlign:"center", paddingTop:25, paddingLeft:150, paddingRight:150}}>
                    <a href={animalList[id]['link']} style={{color:'BlanchedAlmond'}} target="_blank">Click here</a>{' '}
                    to learn more about {animalList[id]['name']}, 
                    including information on how you can adopt {animalList[id]['gender']=='Male' ? 'him' : 'her'}!
                </h2>
                <h5 style={{color:"BlanchedAlmond", textAlign:"center",  paddingTop:25, paddingBottom:100, paddingLeft:150, paddingRight:150}}>Source: {animalList[id]['link']}<br/></h5>
            
            <footer style={{color:"BlanchedAlmond", textAlign:"center",  paddingBottom:100}}>
                <h3>Other Featured Pets:</h3>
                <Container fluid>
                    <Row>
                        {featuredNames}
                    </Row>
                    <Row>
                        {featuredIMGs}
                    </Row>
                </Container>
            </footer>
        </div>
    );
}

export default Instance;