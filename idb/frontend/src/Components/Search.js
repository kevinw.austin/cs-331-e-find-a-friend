import React, {useEffect,useState} from 'react'
import InputGroup from "react-bootstrap/InputGroup"
import FormControl from "react-bootstrap/FormControl"
import DropdownButton from "react-bootstrap/DropdownButton"
import Dropdown from "react-bootstrap/Dropdown"
import Button from "react-bootstrap/Button"

const Search = () => {

    useEffect(() => {

    },[])

    return (
        <div>
            <InputGroup>
                <FormControl
                placeholder="Search"
                aria-label="Search"
                aria-describedby="basic-addon2"
                />

                <DropdownButton
                as={InputGroup.Append}
                variant="outline-secondary"
                title="Dropdown"
                id="input-group-dropdown-2"
                >
                <Dropdown.Item href="#">Breed</Dropdown.Item>
                <Dropdown.Item href="#">Age</Dropdown.Item>
                <Dropdown.Item href="#">Time in Shelter</Dropdown.Item>
                {/* <Dropdown.Divider /> */}
                <Dropdown.Item href="#">Behavior</Dropdown.Item>
                </DropdownButton>
                <Button as="input" type="submit" value="Submit" />
            </InputGroup>
        </div>
    )
}

export default Search